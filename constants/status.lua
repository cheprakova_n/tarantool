return {
    incorrect = {400, 'Body incorrect'},
    not_found = {404, 'Key %s not found'},
    duplicate = {409, 'Key %s alredy exist'},
    req_limit = {429, 'Number of requests to the server has been exceeded'}
}
