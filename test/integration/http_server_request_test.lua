local t = require('luatest')
local http_client = require('http.client')
local json = require('json')
local URL = 'http://localhost:8080'

local g = t.group('integration_api')

g.before_all = function()
end

g.after_each = function()
end

g.test_get404 = function()
    -- удалим запись test
    http_client.delete(URL..'/kv/test')

    -- попытка получить запись test
    local r = http_client.get(URL..'/kv/test')
    local body = json.decode(r.body)
    t.assert_equals(r.status, 404, '/kv/test code')
    t.assert_equals(body.status, false, '/kv status')
end

g.test_post400 = function()
    -- в body не передаем value
    local r = http_client.post(URL..'/kv', 'body')
    local body = json.decode(r.body)
    t.assert_equals(r.status, 400, '/kv code')
    t.assert_equals(body.status, false, '/kv status')

    -- в value передаем некорректный json
    local data = json.encode({key = 'test', value = "[1]]"})
    local r = http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}})
    local body = json.decode(r.body)
    t.assert_equals(r.status, 400, '/kv code')
    t.assert_equals(body.status, false, '/kv status')
end

g.test_post200 = function()
    -- удалим запись test
    http_client.delete(URL..'/kv/test')

    -- добавляем запись test
    local data = json.encode({key = 'test', value = json.encode({data = 'some data'})})
    local r = http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}})
    local body = json.decode(r.body)
    t.assert_equals(r.status, 200, '/kv code')
    t.assert_equals(body.status, true, '/kv status')
end

g.test_post409 = function()
    -- добавим запись test
    local data = json.encode({key = 'test', value = json.encode({data = 'some data'})})
    http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}
    })

    -- попытка повторного добавления записи test
    local r = http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}})
    local body = json.decode(r.body)
    t.assert_equals(r.status, 409, '/kv code')
    t.assert_equals(body.status, false, '/kv status')
end

g.test_get200 = function()
    -- добавим запись test
    local data = json.encode({key = 'test', value = json.encode({data = 'some data'})})
    http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}
    })

    -- получаем данные для test
    local r = http_client.get(URL..'/kv/test')
    local body = json.decode(r.body)
    t.assert_equals(r.status, 200, '/kv/test code')
    t.assert_equals(body.status, true, '/kv/test status')
    t.assert_equals(type(body.value), 'string', '/kv/test value')
    t.assert_equals(body.value, json.encode({data = 'some data'}), '/kv/test value')
end

g.test_put400 = function()
    -- добавим запись test
    local data = json.encode({key = 'test', value = json.encode({data = 'some data'})})
    http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}
    })

    -- в body не передаем value
    local data = json.encode({value1 = json.encode({data = 'new some data'})})
    local r = http_client.put(URL..'/kv/test', data, {
        headers = {['Content-type'] = 'application/json'}})
    local body = json.decode(r.body)
    t.assert_equals(r.status, 400, '/kv/test code')
    t.assert_equals(body.status, false, '/kv/test status')

    -- в value передаем некорректный json
    data = json.encode({value = '[1]]'})
    r = http_client.put(URL..'/kv/test', data, {
        headers = {['Content-type'] = 'application/json'}})
    local body = json.decode(r.body)
    t.assert_equals(r.status, 400, '/kv/test code')
    t.assert_equals(body.status, false, '/kv/test status')
end

g.test_put200 = function()
    -- добавим запись test
    local data = json.encode({key = 'test', value = json.encode({data = 'some data'})})
    http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}
    })

    -- обновим запись test
    local data = json.encode({value = json.encode({data = 'new some data'})})
    local r = http_client.put(URL..'/kv/test', data, {
        headers = {['Content-type'] = 'application/json'}})
    local body = json.decode(r.body)
    t.assert_equals(r.status, 200, '/kv/test code')
    t.assert_equals(body.status, true, '/kv/test status')
end

g.test_put404 = function()
    -- удалим запись test
    http_client.delete(URL..'/kv/test')

    -- попытка обновить запись test
    local data = json.encode({value = json.encode({data = 'new some data'})})
    local r = http_client.put(URL..'/kv/test', data, {
        headers = {['Content-type'] = 'application/json'}})
    local body = json.decode(r.body)
    t.assert_equals(r.status, 404, '/kv/test code')
    t.assert_equals(body.status, false, '/kv/test status')
end

g.test_delete404 = function()
    -- удалим запись test
    http_client.delete(URL..'/kv/test')

    -- повторная попытка удалить запись test
    local r = http_client.delete(URL..'/kv/test')
    local body = json.decode(r.body)
    t.assert_equals(r.status, 404, '/kv/test code')
    t.assert_equals(body.status, false, '/kv/test status')
end

g.test_delete200 = function()
    -- добавим запись test
    local data = json.encode({key = 'test', value = json.encode({data = 'some data'})})
    http_client.post(URL..'/kv', data, {
        headers = {['Content-type'] = 'application/json'}
    })

    -- удалим запись test
    local r = http_client.delete(URL..'/kv/test')
    local body = json.decode(r.body)
    t.assert_equals(r.status, 200, '/kv/test code')
    t.assert_equals(body.status, true, '/kv/test status')
end

g.test_get429 = function()
    -- запросим данные в цикле для превышения лимита запросов
    local r
    for i = 1, 1000 do
        r = http_client.get(URL..'/kv/test')
    end
    local body = json.decode(r.body)
    t.assert_equals(r.status, 429, '/kv/test code')
    t.assert_equals(body.status, false, '/kv/test status')
end
