#!/usr/bin/env tarantool

-- настройка базы данных
box.cfg {
   listen = 3301,
}
local kv = box.schema.space.create('kv', {
   format = {
      {'key', 'string'},
      {'value', 'string'}
   },
   if_not_exists = true})
kv:create_index('primary', {parts = {{'key'}}, if_not_exists = true})

-- создание сервера и настройка маршрутизации
local handler = require('modules.handler')
local server  = require('http.server').new(nil, 8080)
server:route({path = '/kv/:key', method = 'GET'},    function(req) return handler.main(req, handler.get) end )
server:route({path = '/kv',      method = 'POST'},   function(req) return handler.main(req, handler.add) end )
server:route({path = '/kv/:key', method = 'PUT'},    function(req) return handler.main(req, handler.update) end )
server:route({path = '/kv/:key', method = 'DELETE'}, function(req) return handler.main(req, handler.delete) end )
server:start()
