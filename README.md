# tarantool key-value storage



## Описание

Http приложение для организации key-value хранилища, доступ к данным осуществляется через API. В качестве сервера приложения используется [Tarantool](https://www.tarantool.io).

## Установка

- [ ] Установите Tarantool [со страницы установки](https://tarantool.io/ru/download).
- [ ] Склонируйте репозиторий:

```
git clone git@gitlab.com:cheprakova_n/tarantool.git

```

- [ ] Установите http-модуль в папку с репозиторием:

```
tarantoolctl rocks install http

```
- [ ] Установите модуль luatest в папку с репозиторием:

```
tarantoolctl rocks install luatest

```

## Использование

Для запуска сервера передаем имя файла init.lua в Tarantool

```
tarantool init.lua

```

Готово! Для управления данными используется REST API запросы: GET, POST, PUT, DELETE.

Примеры запросов и ответов:

**POST**

Добавим данные, где ключ "test", а value - json

```
curl -d '{"key":"test", "value":"[{\"data\": \"some data\"}]"}' -H "Content-Type: application/json" -X POST http://localhost:8080/kv

```
Ответ:
```
{"status":true}

```

**GET**

Запрос для получения данных по ключу test

```
curl -X GET http://localhost:8080/kv/test

```
Ответ:
```
{"status":true,"value":"[{\"data\": \"some data\"}]"}

```

**PUT**

Для редактирования данных по ключу test

```
curl -d '{"value":"[{\"data\": \"new some data\"}]"}' -H "Content-Type: application/json" -X PUT http://localhost:8080/kv/test

```

Ответ:
```
{"status":true}

```

**DELETE**

Удаление данных по ключу test

```
curl -X DELETE http://localhost:8080/kv/test

```

Ответ:
```
{"status":true}

```

Если запрос выполнился с ошибкой, на любой из запросов GET, POST, PUT, DELETE придет ответ вида:

```
{"status":false,"error":"error message"}

```

## Тесты

Для запуска тестов в консоли запустить команду luatest

```
.rocks/bin/luatest

```
