local log = require('log')
local json = require('json')
local STATUS = require('constants.status')
local MAX_REQUESTS = 100

-- Формирует ответ об ошибке в операции
local function answer_error(status, str)
    local code = status and status[1] or 500
    local msg  = status and string.format(status[2], str) or code

    log.info(string.format('Error: %s', msg))
    return {body = json.encode({status = false, error = msg}), status = code}
end

-- Формирует ответ об успешной операции
local function answer_ok(message, value)
    log.info(message)
    return {body = json.encode({status = true, value = value}), status = 200}
end

-- Проверяет существование ключа в бд
local function is_key_exist(key)
    local res, err = box.space.kv:count(key)
    return (res > 0)
end

-- Проверяет не превышен ли лимит на количество запросов
local function check_limit_requests()
    local stat = box.stat()
    local rps = 0
    for k, v in pairs(stat) do
        rps = rps + v.rps
    end

    if rps > MAX_REQUESTS then
        return false, answer_error(STATUS.req_limit)
    end
    return true
end

local function is_correct_body(key, value)
    if (not key or type(key) ~= 'string' or not value or type(value) ~= 'string') then
        return false, answer_error(STATUS.incorrect)
    end

    local res, err = pcall(json.decode, value)
    if not res then
        return false, answer_error(STATUS.incorrect)
    end
    return true
end

-- Получает одну запись из бд по ключу
local function get(req)
    local key = req:stash('key')
    local res = box.space.kv:get{key}

    -- если ключ не найден
    if not res then
        return answer_error(STATUS.not_found, key)
    end
    
    return answer_ok(string.format('OK: GET kv key %s', key), res[2])
end

-- Добавляет запись в бд
local function add(req)
    local key   = req:post_param('key')
    local value = req:post_param('value')

    -- если некорректные параметры
    local res, err = is_correct_body(key, value)
    if not res then
        return err
    end

    if is_key_exist(key) then
        return answer_error(STATUS.duplicate, key)
    end

    box.space.kv:insert{key, value}

    return answer_ok(string.format('OK: POST kv key=%s value=%s', key, json.encode(value)))
end

-- Обновлеят запись по ключу
local function update(req)
    local key   = req:stash('key')
    local value = req:post_param('value')

    -- если некорректные параметры
    local res, err = is_correct_body(key, value)
    if not res then
        return err
    end
    
    res = box.space.kv:update(key, {{'=', 2, value}})

    -- если ключ не найден
    if not res then
        return answer_error(STATUS.not_found, key)
    end

    return answer_ok(string.format('OK: PUT kv %s', json.encode(res)))
end

-- Удаляет запись по ключу
local function delete(req)
    local key = req:stash('key')

    if not is_key_exist(key) then
        return answer_error(STATUS.not_found, key)
    end

    box.space.kv:delete{key}
    return answer_ok(string.format('OK: DELETE kv key=%s', json.encode(key)))
end

local function handler(req, func)
    if not func then
        return answer_error()
    end

    local res, err = check_limit_requests()
    if not res then
        return err
    end

    return func(req)
end

return {
    main    = handler,
    get     = get,
    add     = add,
    update  = update,
    delete  = delete
}